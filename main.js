/* Starting Plain JavaScript Concept Learning */
//Step 1
console.log("Hey ! Starting page loading process from page.");

// Step 2
var a1;
console.log("The value of a1 is " + a1); // logs "The value of a is undefined"
//console.log("The value of b is " + b); // throws ReferenceError exception

// Step 3
var input;
if (input === undefined) {
	doThis();
} else {
	doThat();
}

function doThis(){
	console.log("doThis function got called.");
}

function doThat(){
	console.log("doThat function got called.")
}



// Step 4
var myArray = [];
if (!myArray[0]) myFunction();


function myFunction(){
	console.log("myFunction method got called, because in boolean expression undefined will work as false case.")
}

// Step 5
var a2;
console.log("Vvalue of a2 is "+a2);

console.log("The undefined value converts to NaN when used in numeric context and hence value of a2 is " + (a2 + 2));

// Step 6




