function DumpAll_16thSept2015() {


	// Data structures and types
	/*
		Six data types that are primitives:
		Boolean. true and false.
		null. A special keyword denoting a null value. Because JavaScript is case-sensitive, null is not the same as Null, NULL, or any other variant.
		undefined. A top-level property whose value is undefined.
		Number. 42 or 3.14159.
		String. "Howdy"
	*/


	// Object literals
	// An object literal is a list of zero or more pairs of property names and associated values of an object, enclosed in curly braces ({})

	var Sales = "Toyota";

	function CarTypes(name) {
		if (name == "Honda") {
			return name;
		} else {
			return "Sorry, we don't sell " + name + ".";
		}
	}

	var car = {
		myCar: "Saturn",
		getCar: CarTypes("Honda"),
		special: Sales
	};

	console.log(car.myCar); // Saturn
	console.log(car.getCar); // Honda
	console.log(car.special); // Toyota



	// Create an object type UserException
	function UserException(message) {
		this.message = message;
		this.name = "UserException";
	}

	// Make the exception convert to a pretty string when used as a string 
	// (e.g. by the error console)
	UserException.prototype.toString = function() {
		return this.name + ': "' + this.message + '"';
	}

	// Create an instance of the object type and throw it
	throw new UserException("Value too high");



	//an be anonymous;
	var square = function(number) {
		return number * number
	};
	var x = square(4) // x gets the value 16


	var factVal = function fac(n) {
		return n < 2 ? 1 : n * fac(n - 1)
	};
	console.log("Factorial vaue of Give NUmber :" + factVal(7));


	// Function expressions are convenient when passing a function as an argument to another function. The following example shows a map function being defined and then called with an anonymous function as its first parameter:

	function map(f, a) {
		var result = [],
			i;
		for (i = 0; i != a.length; i++) {
			result[i] = f(a[i]);
		}
		return result;
	}

	//var val = map(function(x) {return x * x * x}, [0, 1, 2, 5, 10]);
	var val = map(function fac(n) {
		return n < 2 ? 1 : n * fac(n - 1)
	}, [0, 1, 2, 5, 10]);
	console.log("Passing function as an argument and called by anonymus  -->" + val);



	// Condition base Function defination
	var myFunc;

	if (num == 0) {
		myFunc = function(thObject) {
			thObject.make = "Toyota"
		}
	}

	// Function Scope
	// Variables defined inside a function cannot be accessed from anywhere outside the function, because the variable is defined only in the scope of the function.



	// A function defined in the global scope can access all variables defined in the global scope. 

	// A function defined inside another function can also access all variables defined in its parent function and any other variable to which the parent function has access.


	var num1 = 20,
		num2 = 3,
		name = "Rozertou ";

	function multiply() {
		return num1 * num2;
	}

	console.log("Calling Global function ---->" + multiply());

	// A nested Function 

	function getScore() {
		// body...
		var num1 = 67,
			num2 = 678;

		function add() {
			console.log(name + "Scored " + (num1 + num2));
			return name + "Scored " + (num1 + num2);
		}
		return add();
	}

	getScore();



	// Function : Recursion - Getting all node of DOM tree

	function walkTree(node) {
		if (node == null) {
			return;
		}
		for (var i = 0; i < node.childNodes.length; i++) {
			walkTree(node.childNodes[i]);
		}
	}

	// Inner function is private to its containing (outer) function
	// Sample

	function addSquare(a, b) {
		function square(x) {
			return x * x;
		}
		return square(a) + square(b);
	}


	a = addSquare(23, 25);
	console.log(" addSquare Value for a --->" + a);

	b = addSquare(3, 5);
	console.log(" addSquare Value for b --->" + b);

	c = addSquare(2, 5);
	console.log(" addSquare Value for c --->" + c);



} // End of DumpAll_16thSept2015 Function


// Function : Nested Function and Clouser

// Clouser - A closure is an expression (typically a function) that can have free variables together with an environment that binds those variables (that "closes" the expression).

function outside(x) {
	function inside(y) {
		return x + y;
	}
	return inside;
}
fn_inside = outside(3); // Think of it like: give me a function that adds 3 to whatever you give it
result = fn_inside(5); // returns 8

result1 = outside(3)(5); // returns 8
































////
//