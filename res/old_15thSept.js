function DumpAll_15thSept2015() {

	//  1
	var a;
	console.log("The value of a is " + a); // logs "The value of a is undefined"
	console.log("The value of b is " + b); // throws ReferenceError exception

	//  2
	var input;
	if (input === undefined) {
		doThis();
	} else {
		doThat();
	}

	function doThis() {
		console.log("doThis Method called");
	}

	function doThat() {
		console.log("doThat Method called");
	}


	//  3
	var myArray = [];
	if (!myArray[0])
		myFunction();

	function myFunction() {
		console.log("myFunction called");
	}


	//  4
	var a;
	console.log("Value of a --> " + a);
	console.log("undefined value converts to NaN when used in numeric context :   [a + 2] --> " + (a + 2));

	// 5
	var n = null;
	console.log(n * 32); // Will log 0 to the console
	if (!n)
		myFunction();

	function myFunction() {
		console.log("myFunction called");
	}


	// 5 : Variable Scope - Global or Function
	var gv = 12;
	console.log("1. myFunction Global with gv = " + gv);

	myFunction();

	function myFunction() {
		var lv = 352;
		var inlv = myInnerFunction();
		console.log("2.2 myInnerFunction called Local with inlv = " + inlv);
		console.log("2. myFunction called Local with lv = " + lv);
		console.log("3. myFunction called Local with gv = " + gv);

		function myInnerFunction() {
			var a = 3;
			console.log("2.1 myInnerFunction called Local with a = " + a);
			return a;
		}
	}

	console.log("4. Global gv = " + gv);
	console.log("5. Global lv = " + lv);
	console.log("2.3 myInnerFunction called Local with inlv = " + lv);



	// 6 : Variable Scoping  Ex : 1
	var name = "Richard";
	console.log("Name from Global Scope " + name); // Richard: the global variable

	function showName() {
		var name = "Jack"; // local variable; only accessible in this showName function
		console.log("Name from function Scope " + name); // Jack
	}
	showName();
	console.log("Name from Global Scope " + name); // Richard: the global variable



	// 6 : Variable Scoping  Ex : 2
	var name = "Richard";
	console.log("Name from Global Scope ---> " + name); // the blocks in this if statement do not create a local context for the name variable
	if (name) {
		name = "Jack"; // 
		console.log("Name from if Scope is the global name variable and it is being changed to 'Jack' here ---->" + name); // Jack: still the global variable
	}

	// Here, the name variable is the same global name variable, but it was changed in the if statement
	console.log("Name from Global Scope ---->" + name); // Jack



	// 6 : Variable Scoping  Ex : 3
	// If you don't declare your local variables with the var keyword, they are part of the global scope
	var name = "Michael Jackson";

	function showCelebrityName() {
		console.log(" showCelebrityName function called then Name ----------> " + name);
	}

	// The solution is to declare your local variable with the var keyword
	function showOrdinaryPersonName() {
		//console.log (" showOrdinaryPersonName function called tbefore declartion of local variable Name ----------> " + name);
		var name = "Johnny Evers"; // Now name is always a local variable and it will not overwrite the global variable
		console.log(" showOrdinaryPersonName function called then Name ----------> " + name);
	}

	showCelebrityName();
	showOrdinaryPersonName();
	showCelebrityName();


	// 6 : Variable Scoping  Ex : 4
	// Local Variables Have Priority Over Global Variables in Functions

	var name = "Paul";
	console.log(" Global Name ----------> " + name);

	function getusers() {
		// Here, the name variable is local and it takes precedence over the same name variable in the global scope
		var name = "Jack";

		// The search for name starts right here inside the function before it attempts to look outside the function in the global scope
		console.log(" getusers function called then Name ----------> " + name);
	}

	getusers(); // Jack


	// 6 : Variable Scoping  Ex : 5
	// setTimeout Variables are Executed in the Global Scope
	// The use of the "this" object inside the setTimeout function refers to the Window object, not to myObj

	var highValue = 656;
	var constantVal = 112;
	var myObj = {
		highValue: 20,
		constantVal: 5,
		calculateIt: function() {
			setTimeout(function() {
				console.log("Value taken in case of setTimeout function call highValue ---->" + highValue);
				console.log("Value taken in case of setTimeout function call constantVal ---->" + constantVal);
				console.log("Value taken in case of setTimeout function call ---->" + this.constantVal * this.highValue);
			}, 2500);
		}
	}

	// The "this" object in the setTimeout function used the global highValue and constantVal variables, because the reference to "this" in the setTimeout function refers to the global window object, not to the myObj object as we might expect.

	myObj.calculateIt(); // 400
	// This is an important point to remember.



	// 7 : Variable hoisting  Ex : 1
	// use variable first and then refer to that variable later 
	// variables in JavaScript can refer to a variable declared later, without getting an exception 

	console.log("Value of x -->" + x);
	console.log(x === undefined); // logs "true"

	// variables that are hoisted will return a value of undefined,
	// So even if you declare and inititalize after you use or refer to this variable, it will still return undefined.

	var x = 3;
	console.log("Value of variable hoisting x -->" + x); // logs "true"



	// 7 : Variable hoisting  Ex : 2
	// Simple demonstration of function scope and Variable Hoisting Problem.
	var n = 10000;
	console.log("6. Value of N outside function is " + n);

	function printSomething() {
		console.log("6. Value of N is " + n);
		var n = 2;
		console.log("6. Value of N is " + n);
	}
	printSomething();



	// 7 : Variable hoisting  Ex : 3
	// Simple demonstration of function scope and Variable Hoisting Problem.
	m = 1;
	console.log("Outside function m is declared without var and Value of m =  " + m);

	function printSomething2() {
		console.log("Value of M is " + m);
		var m = 2;
		console.log("Value of M is " + m);
	}
	printSomething2();


	// 7 : Variable hoisting  Ex : 4
	function showName() {
		console.log("First Name: " + name);
		var name = "Ford";
		console.log("Last Name: " + name);
	}

	showName();
	// First Name: undefined
	// Last Name: Ford

	/*
		// The reason undefined prints first is because the local variable name was hoisted(lifted and declared)  to the top of the function
		// Which means it is this local variable that get calls the first time.
		// This is how the code is actually processed by the JavaScript engine:

		function showName() {
			var name; // name is hoisted (note that is undefined at this point, since the assignment happens below)
			console.log("First Name: " + name); // First Name: undefined

			name = "Ford"; // name is assigned a value

			// now name is Ford
			console.log("Last Name: " + name); // Last Name: Ford
		}
	*/

	// 7 : Variable Scoping  Ex : 5
	// Function Declaration Overrides Variable Declaration When Hoisted
	// Both function declaration and variable declarations are hoisted to the top of the containing scope (but not over variable assignment)

	// Both the variable and the function are named myName
	var myName;

	function myName() {
		console.log("Rich");
	}

	// The function declaration overrides the variable name
	console.log(typeof myName); // function

} // End of DumpAll_15thSept2015 Function
